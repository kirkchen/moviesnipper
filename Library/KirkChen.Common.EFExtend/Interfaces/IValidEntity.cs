﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KirkChen.Common.EFExtend.Interfaces
{
    /// <summary>
    /// IValidEntity
    /// </summary>
    public interface IValidEntity
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </value>
        bool IsValid { get; set; }
    }
}
