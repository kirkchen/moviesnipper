﻿using EFHooks;
using KirkChen.Common.EFExtend.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KirkChen.Common.EFExtend.Hooks
{
/// <summary>
/// Use Valid Flag Instead of Truely Remove Object
/// </summary>
public class UseValidInsteadOfDeleteHook : PreDeleteHook<IValidEntity>
{
    /// <summary>
    /// Hooks the specified entity.
    /// </summary>
    /// <param name="entity">The entity.</param>
    /// <param name="metadata">The metadata.</param>
    /// <exception cref="System.NotImplementedException"></exception>
    public override void Hook(IValidEntity entity, HookEntityMetadata metadata)
    {
        entity.IsValid = false;

        metadata.CurrentContext.Entry(entity).State = EntityState.Modified;
    }

    /// <summary>
    /// Gets a value indicating whether [requires validation].
    /// </summary>
    /// <value>
    ///   <c>true</c> if [requires validation]; otherwise, <c>false</c>.
    /// </value>
    /// <exception cref="System.NotImplementedException"></exception>
    public override bool RequiresValidation
    {
        get { return false; }
    }
}
}
