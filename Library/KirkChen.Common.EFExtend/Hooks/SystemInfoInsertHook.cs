﻿using EFHooks;
using KirkChen.Common.EFExtend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KirkChen.Common.EFExtend.Hooks
{
    /// <summary>
    /// Update System Info Before Insert Into Database
    /// </summary>
    public class SystemInfoInsertHook : PreInsertHook<ISystemInfo>
    {
        /// <summary>
        /// The user getter
        /// </summary>
        private Func<string> userGetter;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemInfoInsertHook"/> class.
        /// </summary>
        /// <param name="userGetter">The user getter.</param>
        public SystemInfoInsertHook(Func<string> userGetter)
        {
            this.userGetter = userGetter;
        }

        /// <summary>
        /// Hooks the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="metadata">The metadata.</param>
        public override void Hook(ISystemInfo entity, HookEntityMetadata metadata)
        {
            var createdUser = "Unknown";
            if (this.userGetter != null)
            {
                createdUser = this.userGetter();
            }

            entity.CreatedAt = DateTime.Now;
            entity.CreatedBy = createdUser;
            entity.UpdatedAt = DateTime.Now;
            entity.UpdatedBy = createdUser;
        }

        /// <summary>
        /// Gets a value indicating whether [requires validation].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [requires validation]; otherwise, <c>false</c>.
        /// </value>
        public override bool RequiresValidation
        {
            get
            {
                return false;
            }
        }
    }
}
