﻿using KirkChen.Common.EFExtend.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieSnipper.Repository.Tables.Models
{
    /// <summary>
    /// EntityBase
    /// </summary>
    public class EntityBase : IValidEntity, ISystemInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityBase"/> class.
        /// </summary>
        public EntityBase()
        {
            this.IsValid = true;
        }

        public bool IsValid { get; set; }

        public DateTime CreatedAt { get; set; }

        public string CreatedBy { get; set; }

        public DateTime UpdatedAt { get; set; }

        public string UpdatedBy { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
