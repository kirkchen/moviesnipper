﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieSnipper.Repository.Tables.Models
{
    /// <summary>
    /// Movie
    /// </summary>
    public class Movie : EntityBase
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [Key]        
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [StringLength(100)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the movie length. (minutes)
        /// </summary>
        /// <value>
        /// The movie length.
        /// </value>
        [Required]
        public int Length { get; set; }

        /// <summary>
        /// Gets or sets the release date.
        /// </summary>
        /// <value>
        /// The release date.
        /// </value>        
        public DateTime? ReleaseDate { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>  
        [Required]
        public string Description { get; set; }
    }
}
