﻿using EFHooks;
using KirkChen.Common.EFExtend.Hooks;
using MovieSnipper.Repository.Tables.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieSnipper.Repository.Tables.Contexts
{
    public class MovieContext : HookedDbContext
    {
        public MovieContext()
            : base()
        {
            this.RegisterHook(new SystemInfoInsertHook(null));
            this.RegisterHook(new SystemInfoUpdateHook(null));
            this.RegisterHook(new UseValidInsteadOfDeleteHook());    
        }

        public IDbSet<Movie> Movies { get; set; }
    }
}
